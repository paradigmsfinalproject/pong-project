import os, sys, pygame, math, json
from twisted.internet.protocol import Protocol, ClientFactory, Factory
from twisted.protocols.basic import LineReceiver
from twisted.internet.tcp import Port
from twisted.internet import task
from twisted.internet import reactor
from twisted.internet.defer import DeferredQueue

from board import *

PLAYER1_PORT = 50011
PLAYER2_PORT = 50012


class ServerGS(object):
	def __init__(self):
		self.startGame = False

	def start(self):

		pygame.init()
                self.player = 1
		gs.moving = "no"
                self.incrementRightScore = False
                self.incrementLeftScore = False

                self.leftPaddle = LeftPaddle(self)
                self.rightPaddle = RightPaddle(self)
                self.leftScore = LeftScore(self)
                self.rightScore = RightScore(self)
                self.centerline = centerLine(self)

		self.ball = Ball(self)
		reactor.callLater(0.02, self.tick)

		self.D = {}

	def updateLeftPaddle(self, data):
		if data == 'up':
			self.leftPaddle.rect = self.leftPaddle.rect.move(0, -10)
		elif data == 'down':
			self.leftPaddle.rect = self.leftPaddle.rect.move(0,10)

	def updateRightPaddle(self, data):
		if data == 'up':
			self.rightPaddle.rect = self.rightPaddle.rect.move(0, -10)
		elif data == 'down':
			self.rightPaddle.rect = self.rightPaddle.rect.move(0,10)

	def tick(self):
		self.ball.tick()
		self.rightScore.tick()
		self.leftScore.tick()
		self.D['x'] = self.ball.rect.x
		self.D['y'] = self.ball.rect.y
		self.D['rp'] = self.rightPaddle.rect.y
		self.D['lp'] = self.leftPaddle.rect.y
		self.D['rs'] = self.rightScore.num
		self.D['ls'] = self.leftScore.num
		self.D['moving'] = self.moving
		self.connection1.sendLine(json.dumps(self.D))
		self.connection2.sendLine(json.dumps(self.D))

def is_json(myjson):
        try:
                json_object = json.loads(myjson)
        except ValueError, e:
                return False
        return True


class PlayerOneConnection(Protocol, LineReceiver):

	def __init__(self, addr):
		self.addr = addr

	def connectionMade(self):
		gs.connection2 = self
		print gs.connection2
		self.queue1 = DeferredQueue()
		self.queue2 = DeferredQueue()

		factory = PlayerTwoConnectionFactory(self.queue1, self.queue2)
		reactor.listenTCP(PLAYER2_PORT, factory)
		print 'Player 1 Connection Made:', self.transport.getPeer()

	def dataReceived(self, data):
		if gs.startGame == True:
			data = data.split(self.delimiter)
			for d in data:
				if (is_json(d) == True):
					d= json.loads(d)
					pause = d['pause']
					user=d['user']
					if user == "one":
						rp = d['rp']
						gs.rightPaddle.rect.y = rp
					elif user =="two":
						lp=d['lp']
						gs.leftPaddle.rect.y = lp
					if d['spacePressed'] == "yes":
						print 'changing inMotion line 124'
						if gs.ball.inMotion == False:
							gs.ball.inMotion = True
						else:
							gs.ball.inMotion = False

			if pause == "yes":
				gs.Paused = True
			else:
				gs.Paused = False

	def connectionLost(self, reason):
		print 'Player 1 Connection Lost:', self.transport.getPeer()

		reactor.stop()

class PlayerOneConnectionFactory(ClientFactory):

	def buildProtocol(self, addr):
		return PlayerOneConnection(addr)

class PlayerTwoConnection(Protocol, LineReceiver):

	def __init__(self, queue1, queue2):
		self.queue2= queue2
		self.queue1= queue1
	

	def connectionMade(self):
		gs.startGame = True
		print 'Player 2 Connection Made:', self.transport.getPeer()
		self.queue2.get().addCallback(self.playerOneDataReceived)
		gs.connection1 = self
		gs.start()
		l=task.LoopingCall(self.runEverySecond)
		l.start(.01)

	def playerOneDataReceived(self, data):
		if self.queue2:
			self.sendLine(data)
			self.queue2.get().addCallback(self.playerOneDataReceived)
		else:
			self.factory.queue2.put(data)
	def dataReceived(self, data):
		data = data.split(self.delimiter)
		for d in data:
			if (is_json(d) == True):
				d= json.loads(d)
				pause = d['pause']
				user=d['user']
				if user == "one":
					rp = d['rp']
					gs.rightPaddle.rect.y = rp
				elif user =="two":
					lp=d['lp']
					gs.leftPaddle.rect.y = lp
				if d['spacePressed'] == "yes":
					if gs.ball.inMotion == False:
						gs.ball.inMotion = True
					else:
						gs.ball.inMotion = False

		if pause == "yes":
			gs.Paused = True
		else:
			gs.Paused = False
		self.queue1.put(data)

	def connectionLost(self, reason):
		print 'Player 2 Connection Lost:', self.transport.getPeer()
		if self.queue2:
			self.queue2= None

	def runEverySecond(self):
		gs.tick()

class PlayerTwoConnectionFactory(ClientFactory):

	def __init__(self, queue1, queue2):
		self.queue1 = queue1
		self.queue2=queue2
		
	def buildProtocol(self, addr):
		return PlayerTwoConnection(self.queue1, self.queue2)


gs= ServerGS()
reactor.listenTCP(PLAYER1_PORT, PlayerOneConnectionFactory())
reactor.run()


