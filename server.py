#Brittany Harrington and Elizabeth Vista
#CSE 30332
#Final Project -- Pong
#Server.py -- server portion of the game. Creates connections to two players and contains a game state that is updated and sent out to the two clients.

import os, sys, pygame, math, json
from twisted.internet.protocol import Protocol, ClientFactory, Factory
from twisted.protocols.basic import LineReceiver
from twisted.internet.tcp import Port
from twisted.internet import task
from twisted.internet import reactor
from twisted.internet.defer import DeferredQueue

from board import *
#from board.py import LeftPaddle, RightPaddle, LeftScore, RightScore

PLAYER1_PORT = 50011
PLAYER2_PORT = 50012



#The ServerGS object is the game state that exists in the server. It keeps track of the ball and paddle coordinates, scores and state of the game (pause and ball in motion). The functions from Board.py are called here in the tick function to update the board state and send it to both players.
class ServerGS(object):
	def __init__(self):
		#do not start game until both players are connected
		self.startGame = False

	def start(self):

		#when both players connected, initialize variables and objects on game screen
		pygame.init()
                self.player = 1
		self.Paused = False
		gs.moving = "no"
                self.incrementRightScore = False
                self.incrementLeftScore = False

                self.leftPaddle = LeftPaddle(self)
                self.rightPaddle = RightPaddle(self)
                self.leftScore = LeftScore(self)
                self.rightScore = RightScore(self)
                self.centerline = centerLine(self)


		self.ball = Ball(self)
		reactor.callLater(0.02, self.tick)

		#initialize dictionary to send info to players
		self.D = {}

	#tick function called in game loop
	def tick(self):
		#call tick functions for ball and scores from board.py
		self.ball.tick()
		self.rightScore.tick()
		self.leftScore.tick()

		#populate dictionary with game state information to send to players
		self.D['x'] = self.ball.rect.x
		self.D['y'] = self.ball.rect.y
		self.D['rp'] = self.rightPaddle.rect.y
		self.D['lp'] = self.leftPaddle.rect.y
		self.D['rs'] = self.rightScore.num
		self.D['ls'] = self.leftScore.num
		self.D['pause'] = self.Paused
		self.D['moving'] = self.moving

		#send dictionary to players
		self.connection1.sendLine(json.dumps(self.D))
		self.connection2.sendLine(json.dumps(self.D))

#function to determine if a string is json
def is_json(myjson):
        try:
                json_object = json.loads(myjson)
        except ValueError, e:
                return False
        return True

#class representing connection between player 1 and the server
class PlayerOneConnection(Protocol, LineReceiver):

	def __init__(self, addr):
		self.addr = addr

	#on connection, set the second connection in the game state. Create two differed queues to send information between the server and the two clients
	def connectionMade(self):
		gs.connection2 = self
		self.queue1 = DeferredQueue()
		self.queue2 = DeferredQueue()

		#once connection one is established, be on the lookout for the second player
		#create connection to player 2 with the two queues from player 1
		factory = PlayerTwoConnectionFactory(self.queue1, self.queue2)
		reactor.listenTCP(PLAYER2_PORT, factory)
		print 'Player 1 Connection Made:', self.transport.getPeer()

	#function called when data from player1 is received
	def dataReceived(self, data):
		#only do something with the data if the game has begun
		if gs.startGame == True:
			data = data.split(self.delimiter)
			for d in data:
				if (is_json(d) == True):
					d= json.loads(d)
					#pull necessary information from json dictionary
					pause = d['pause']
					user=d['user']
					#get the right paddle coordinates from player one and left paddle coordinates from player two
					if user == "one":
						rp = d['rp']
						gs.rightPaddle.rect.y = rp
					elif user =="two":
						lp=d['lp']
						gs.leftPaddle.rect.y = lp	
					#if notification that space bar has been pressed, change the ball's motion to true
					if d['spacePressed'] == "yes":
						if gs.ball.inMotion == False:
							gs.ball.inMotion = True
						else:
							gs.ball.inMotion = False

			if pause == "yes":
				gs.Paused = True
			else:
				gs.Paused = False
				
	#stop the reactor when connection one is cancelled
	def connectionLost(self, reason):
		print 'Player 1 Connection Lost:', self.transport.getPeer()

		reactor.stop()

#Factory to create player1 connection
class PlayerOneConnectionFactory(ClientFactory):

	def buildProtocol(self, addr):
		return PlayerOneConnection(addr)

#Class representing connection with Player 2
class PlayerTwoConnection(Protocol, LineReceiver):

	#set queues received from player1 connection
	def __init__(self, queue1, queue2):
		self.queue2= queue2
		self.queue1= queue1
	
	#once player 2 connects, game can begin
	def connectionMade(self):
		gs.startGame = True
		print 'Player 2 Connection Made:', self.transport.getPeer()
		self.queue2.get().addCallback(self.playerOneDataReceived)
		gs.connection1 = self

		#start game loop and looping call
		gs.start()
		l=task.LoopingCall(self.runEverySecond)
		l.start(.01)

	#when data is received from player one, send along to player 2
	def playerOneDataReceived(self, data):
		if self.queue2:
			self.sendLine(data)
			self.queue2.get().addCallback(self.playerOneDataReceived)
		else:
			self.factory.queue2.put(data)

	#when data is received from player 2, update paddle and motion variables to reflect any changes from the client side
	def dataReceived(self, data):
		data = data.split(self.delimiter)
		for d in data:
			if (is_json(d) == True):
				d= json.loads(d)
				pause = d['pause']
				user=d['user']
				if user == "one":
					rp = d['rp']
					gs.rightPaddle.rect.y = rp
				elif user =="two":
					lp=d['lp']
					gs.leftPaddle.rect.y = lp
				if d['spacePressed'] == "yes":
					if gs.ball.inMotion == False:
						gs.ball.inMotion = True
					else:
						gs.ball.inMotion = False

		if pause == "yes":
			gs.Paused = True
		else:
			gs.Paused = False
		self.queue1.put(data)

	#player2 connection lost function
	def connectionLost(self, reason):
		print 'Player 2 Connection Lost:', self.transport.getPeer()
		if self.queue2:
			self.queue2= None
	#game state tick function is called on each iteration of the game
	def runEverySecond(self):
		gs.tick()

#factory to create instance of player2
class PlayerTwoConnectionFactory(ClientFactory):

	def __init__(self, queue1, queue2):
		#establish differed queues from player 1 connection
		self.queue1 = queue1
		self.queue2=queue2
		
	def buildProtocol(self, addr):
		return PlayerTwoConnection(self.queue1, self.queue2)


#initialize game state object and reactor to listen for player 1 connection
gs= ServerGS()
reactor.listenTCP(PLAYER1_PORT, PlayerOneConnectionFactory())
reactor.run()


