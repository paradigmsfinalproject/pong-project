
import sys, os, pygame, math, random
from pygame.locals import *

class LeftPaddle(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs=gs	
		#images are also uploaded onto bitbucket
		self.image=pygame.image.load("paddle.png")
		self.image = pygame.transform.scale(self.image, (150, 150))
		self.rect = self.image.get_rect()
		self.rect.x = 0
		self.rect.y = 200
		self.score = 0
	
	def tick(self):
		pass

class RightPaddle(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs=gs	
		self.image=pygame.image.load("paddle.png")
		self.image = pygame.transform.scale(self.image, (150, 150))
		self.rect = self.image.get_rect()
		self.rect.x = 490
		self.rect.y = 200
		self.score = 0	

	def tick(self):
		pass

class LeftScore(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.num = 0
		self.font = pygame.font.Font(None, 48)
		self.text = self.font.render("0", 1, (255,255,255))
		self.rect = self.text.get_rect()
		self.rect.x = 250
		self.rect.y = 30

	def tick(self):
		if self.gs.incrementLeftScore == True:
			self.num = self.num + 1
			self.text = self.font.render(str(self.num), 1, (255, 255, 255))
			self.gs.incrementLeftScore = False
		

class RightScore(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.num = 0
		self.font = pygame.font.Font(None, 48)
		self.text = self.font.render("0", 1, (255,255,255))
		self.rect = self.text.get_rect()
		self.rect.x = 380
		self.rect.y = 30

	def tick(self):
		if self.gs.incrementRightScore == True:
			self.num = self.num + 1
			self.text = self.font.render(str(self.num), 1, (255, 255, 255))
			self.gs.incrementRightScore = False

class player1Label(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.font = pygame.font.Font(None, 30)
		self.text = self.font.render("Player 1", 1, (255, 255, 255))
		self.rect = self.text.get_rect()
		self.rect.x = 40
		self.rect.y = 30

	def tick(self):
		pass

class player2Label(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.font = pygame.font.Font(None, 30)
		self.text = self.font.render("Player 2", 1, (255, 255, 255))
		self.rect = self.text.get_rect()
		self.rect.x = 520
		self.rect.y = 30

	def tick(self):
		pass

class initial(pygame.sprite.Sprite):
	def __init__(self, gs = None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.font = pygame.font.Font(None, 72)
		self.text = self.font.render("Waiting for Opponent..", 1, (255,255,255))
		self.rect = self.text.get_rect()
		self.rect.x = 100
		self.rect.y = 200

class gameOverWin(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.font = pygame.font.Font(None, 72)
		self.text = self.font.render("Game Over! You Win!!", 1, (255, 255, 255))
		self.rect = self.text.get_rect()
		self.rect.x = 50
		self.rect.y = 200

	def tick(self):
		pass

class gameOverLose(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.font = pygame.font.Font(None, 72)
		self.text = self.font.render("Game Over! You Lose!!", 1, (255, 255, 255))
		self.rect = self.text.get_rect()
		self.rect.x = float(50)
		self.rect.y = float(200)

	def tick(self):
		pass

class centerLine(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.image = pygame.image.load("centerline.png")
		self.image = pygame.transform.scale(self.image, (40, 480))
		self.rect = self.image.get_rect()
		self.rect.x = 310

	def tick(self):
		pass

class Ball(pygame.sprite.Sprite):
	def __init__(self, gs = None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.image = pygame.image.load("ball.png")
		self.rect = self.image.get_rect()
		self.rect.x = 320
		self.rect.y = 240
		self.maxspeed = 4
		self.minspeed = 1
		self.xspeed = random.uniform(self.minspeed, self.maxspeed)
		self.yspeed = random.uniform(self.minspeed, self.maxspeed)
		self.inMotion = False
		self.angle = 0
		self.rangle = 0
	def newAngle(self):
		self.angle = 90
		print "Original angle ", self.angle
		self.rangle = (float(self.angle) / float(360)) * 2 *math.pi
		print "Original rangle ", self.rangle	

	def tick(self):
		if self.inMotion == True:
			if self.rect.x >= 320:
				self.gs.player = 1
			else :
				self.gs.player = 2

			scored = False
			
			x = self.rect.x
			y = self.rect.y	

			
			if x >= 640:
				# Player 2 scores
				self.gs.incrementLeftScore = True
				x = 320
				y = 240
				self.inMotion = False
				self.xspeed = random.uniform(self.minspeed, self.maxspeed)
				self.yspeed = random.uniform(self.minspeed, self.maxspeed)
			
			elif x <= 0:
				# Player 1 scores
				self.gs.incrementRightScore = True
				x = 320
				y = 240
				self.xspeed = random.uniform(self.minspeed, self.maxspeed)
				self.yspeed = random.uniform(self.minspeed, self.maxspeed)
			elif x <= 78 and y >= self.gs.leftPaddle.rect.y + 36 and y <= self.gs.leftPaddle.rect.y + 90:
				self.xspeed = -1 * self.xspeed
			elif x >= 541 and y >= self.gs.rightPaddle.rect.y + 36 and y <= self.gs.rightPaddle.rect.y + 90:
				self.xspeed = -1 * self.xspeed
			if y>= 480:
				self.yspeed = -1 * self.yspeed
			elif y <= 0:
				self.yspeed = -1 * self.yspeed

			x = x + self.xspeed
			y = y + self.yspeed

			self.rect.x = x
			self.rect.y = y	
