import os, sys, pygame, math
from twisted.internet.protocol import Protocol, ClientFactory, Factory
from twisted.protocols.basic import LineReceiver
from twisted.internet.tcp import Port
from twisted.internet import task
from twisted.internet import reactor
from twisted.internet.defer import DeferredQueue

from board import *
#from board.py import LeftPaddle, RightPaddle, LeftScore, RightScore

PLAYER1_PORT = 50011
PLAYER2_PORT = 50012

###############################

class GameState(object):
	def __init__(self, playerOne, playerTwo):
		self.playerOne = playerOne
		self.playerTwo = playerTwo
		pygame.init()
		self.size= width, height = (640, 480)
		self.screen = pygame.display.set_mode(self.size)
		self.black=(0,0,0)
		self.player = 1

		self.incrementRightScore = False
		self.incrementLeftScore = False
	
		self.leftPaddle = LeftPaddle(self)
		self.rightPaddle = RightPaddle(self)
		self.leftScore = LeftScore(self)
		self.rightScore = RightScore(self)
		self.centerline = centerLine(self)
		self.ball = Ball(self)
		reactor.callLater(0.1, self.tick)

	def tick(self):
		print "in tick"
		for event in pygame.event.get():
			if event.type == QUIT:
			#	sys.exit()
				reactor.stop()

#			elif event.type == KEYDOWN:
#				if event.key == K_SPACE:
#					if self.ball.inMotion == False:
#						self.ball.inMotion = True
#					elif self.ball.inMotion == True:
#						self.ball.inMotion = False
#			keys = pygame.key.get_pressed()
#			if keys[pygame.K_UP] != 0:
#				print 'writing up'
#				self.connection.transport.write('up')
#				self.rightPaddle.rect = self.rightPaddle.rect.move(0, -4)
#			if keys[pygame.K_DOWN] != 0:
#				print 'writing down'
#				self.connection.transport.write('down')
#				self.rightPaddle.rect = self.rightPaddle.rect.move(0, 4)
	
		self.leftPaddle.tick()
		self.rightPaddle.tick()
		self.leftScore.tick()
		self.rightScore.tick()
		self.centerline.tick()
		self.ball.tick()

		self.playerOne.transport.write(self)
		self.playerTwo.transport.write(self)

	def spacePressed(self):
		if self.ball.inMotion == False:
			self.ball.inMotion = True
		elif self.ball.inMotion == True:
			self.ball.inMotion = False


	def updateLeftPaddle(self, data):
		if data == 'up':
			self.leftPaddle.rect = self.leftPaddle.rect.move(0, -4)
		elif data == 'down':
			self.leftPaddle.rect = self.leftPaddle.rect.move(0, 4)

	def updateRightPaddle(self, data):
		if data == 'up':
			self.rightPaddle.rect = self.rightPaddle.rect.move(0, -4)
		elif data == 'down':
			self.rightPaddle.rect = self.rightPaddle.rect.move(0, 4)



###############################

class PlayerOneConnection(Protocol):

	def __init__(self, addr):
		self.addr = addr

	def connectionMade(self):
		self.playerTwo = reactor.listenTCP(PLAYER2_PORT, PlayerTwoConnectionFactory(self))
		print 'Player 1 Connection Made:', self.transport.getPeer()

	def dataReceived(self, data):
		if data == 'up' or data == 'down':
			self.gs.updateLeftPaddle(data)
		elif data == 'space':
			self.gs.spacePressed()
		print data

	def connectionLost(self, reason):
		print 'Player 1 Connection Lost:', self.transport.getPeer()
	#	reactor.stop()

class PlayerOneConnectionFactory(ClientFactory):

	def buildProtocol(self, addr):
		return PlayerOneConnection(addr)

class PlayerTwoConnection(Protocol):

	def __init__(self, playerOne):
		self.playerOne = playerOne

	def connectionMade(self):
		print 'Player 2 Connection Made:', self.transport.getPeer()
		self.gs = GameState(self.playerOne, self)
		l=task.LoopingCall(self.gs.tick)
		l.start(0.5)


	def dataReceived(self, data):
		if data == 'up' or data == 'down':
			self.gs.updateRightPaddle(data)
		elif data == 'space':
			self.gs.spacePressed()
		print data

	def connectionLost(self, reason):
		print 'Player 2 Connection Lost:', self.transport.getPeer()

#	def runEverySecond(self):
#		print "a second has passed"
		#self.playerOne.sendLine("send to player 1")
		#self.playerTwo.sendLine("send to player 2")
	#	gs.main()

class PlayerTwoConnectionFactory(ClientFactory):

	def __init__(self, playerOne):
		self.playerOne = playerOne

	def buildProtocol(self, playerOne):
		return PlayerTwoConnection(playerOne)

#def runEverySecond():
#	print "a second has passed"
#	gs.main()



reactor.listenTCP(PLAYER1_PORT, PlayerOneConnectionFactory())
reactor.run()


