
import sys, os, pygame, math, random
from pygame.locals import *

class LeftPaddle(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs=gs	
		#images are also uploaded onto bitbucket
		self.image=pygame.image.load("paddle.png")
		self.image = pygame.transform.scale(self.image, (150, 150))
		self.rect = self.image.get_rect()
		self.rect.x = 0
		self.rect.y = 200
		self.score = 0
	
	def tick(self):
		pass

class RightPaddle(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs=gs	
		self.image=pygame.image.load("paddle.png")
		self.image = pygame.transform.scale(self.image, (150, 150))
		self.rect = self.image.get_rect()
		self.rect.x = 490
		self.rect.y = 200
		self.score = 0	


	def tick(self):
		pass

class LeftScore(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.num = 0
		self.font = pygame.font.Font(None, 48)
		self.text = self.font.render("0", 1, (255,255,255))
		self.rect = self.text.get_rect()
		self.rect.x = 250
		self.rect.y = 30

	def tick(self):
		if self.gs.incrementLeftScore == True:
			self.num = self.num + 1
			self.text = self.font.render(str(self.num), 1, (255, 255, 255))
			self.gs.incrementLeftScore = False
		
#		pass

class RightScore(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.num = 0
		self.font = pygame.font.Font(None, 48)
		self.text = self.font.render("0", 1, (255,255,255))
		self.rect = self.text.get_rect()
		self.rect.x = 380
		self.rect.y = 30

	def tick(self):
		if self.gs.incrementRightScore == True:
			self.num = self.num + 1
			self.text = self.font.render(str(self.num), 1, (255, 255, 255))
			self.gs.incrementRightScore = False
#		pass

class pauseGame(pygame.sprite.Sprite):
	def __init__(self, gs= None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.font = pygame.font.Font(None, 72)
		self.text = self.font.render("Game Paused", 1, (255, 255, 255))
		self.rect = self.text.get_rect()
		self.rect.x = 100
		self.rect.y = 200

	def tick(self):
		pass

class gameOverWin(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.font = pygame.font.Font(None, 72)
		self.text = self.font.render("Game Over! You Win!!", 1, (255, 255, 255))
		self.rect = self.text.get_rect()
		self.rect.x = 50
		self.rect.y = 200

	def tick(self):
		pass

class gameOverLose(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.font = pygame.font.Font(None, 72)
		self.text = self.font.render("Game Over! You Lose!!", 1, (255, 255, 255))
		self.rect = self.text.get_rect()
		self.rect.x = 50
		self.rect.y = 200

	def tick(self):
		pass

class centerLine(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.image = pygame.image.load("centerline.png")
		self.image = pygame.transform.scale(self.image, (40, 480))
		self.rect = self.image.get_rect()
		self.rect.x = 310

	def tick(self):
		pass

class Ball(pygame.sprite.Sprite):
	def __init__(self, gs = None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.image = pygame.image.load("ball.png")
		self.rect = self.image.get_rect()
		self.rect.x = 320
		self.rect.y = 240
		self.speed = 10
		self.inMotion = False
		self.angle = random.randint(0, 360)
		#self.angle = 270
		self.rangle = (float(self.angle) / float(360)) * 2 *math.pi

	def tick(self):
		if self.inMotion == True:
			if self.rect.x >= 320:
				self.gs.player = 1
			else :
				self.gs.player = 2

			scored = False
		#	print rangle

			x = self.rect.x + (math.sin(self.rangle)) *(self.speed)
			y = self.rect.y + math.cos(self.rangle) * (self.speed)
			
			if x >= 640:
				self.gs.incrementLeftScore = True
				x = 320
				y = 240
				self.inMotion = False
				self.angle = random.randint(180, 360)
				self.rangle = (float(self.angle) / float(360)) * 2 *math.pi
			
			elif x <= 0:
				self.gs.incrementRightScore = True
				x = 320
				y = 240
				self.inMotion = False
				self.angle = random.randint(180, 360)
				self.rangle = (float(self.angle) / float(360)) * 2 *math.pi
			elif x <= 70 and y >= self.gs.leftPaddle.rect.y and y <= self.gs.leftPaddle.rect.y + 70:
				self.angle = random.randint(0, 180)
				self.rangle=(float(self.angle) / float(360)) *2 * math.pi
				x = self.rect.x + (math.sin(self.rangle)) *(self.speed)
				y = self.rect.y + math.cos(self.rangle) * (self.speed)
			elif x >= 570 and y >= self.gs.rightPaddle.rect.y and y <= self.gs.rightPaddle.rect.y + 70:
				self.angle = random.randint(180, 360)
				self.rangle=(float(self.angle) / float(360)) *2 * math.pi
				x = self.rect.x + (math.sin(self.rangle)) *(self.speed)
				y = self.rect.y + math.cos(self.rangle) * (self.speed)
			if y>= 480:
				self.angle = random.randint(90, 270)
				self.rangle = (float(self.angle)/float(360)) *2*math.pi
				x = self.rect.x + (math.sin(self.rangle)) *(self.speed)
				y = self.rect.y + math.cos(self.rangle) * (self.speed)
			elif y <= 0:
				if x >= 320:
					self.angle = random.randint(0,90)
				else:
					self.angle = random.randint(270, 360)
				self.rangle = (float(self.angle)/float(360)) *2*math.pi
				x = self.rect.x + (math.sin(self.rangle)) *(self.speed)
				y = self.rect.y + math.cos(self.rangle) * (self.speed)
					
			self.rect.x = x
			self.rect.y = y	


			#check board bounds


#class GameSpace:
#	def main(self):
#		pygame.init()
#		Clock.__init__(self) #added new
#		self.size=width, height = (640,480)
#		self.screen = pygame.display.set_mode(self.size)
#		self.black = (0,0,0)
	
#		self.player=1

#		self.incrementRightScore = False
#		self.incrementLeftScore = False

#		self.clock = pygame.time.Clock()
#		self.leftPaddle = LeftPaddle(self)
#		self.rightPaddle = RightPaddle(self)
#		self.leftScore = LeftScore(self)
#		self.rightScore = RightScore(self)
#		self.centerline = centerLine(self)
#		self.ball = Ball(self)

#		while True:
#			self.clock.tick(60)
#			
#			for event in pygame.event.get():
#				if event.type == QUIT:
#					sys.exit()
#				elif event.type == KEYDOWN:
		#			if event.key == K_DOWN:
		#				self.leftPaddle.rect =self.leftPaddle.rect.move(0,10)
		#			if event.key == K_UP:
		#				self.leftPaddle.rect = self.leftPaddle.rect.move(0, -10)
#					if event.key == K_SPACE:
#						if self.ball.inMotion == False:
#							self.ball.inMotion = True
#						elif self.ball.inMotion == True:
#							self.ball.inMotion =False
#			self.leftPaddle.tick()
#			self.rightPaddle.tick()

#			keys = pygame.key.get_pressed()
#			if keys[pygame.K_UP] != 0:
#				self.leftPaddle.rect = self.leftPaddle.rect.move(0, -4)
#			if keys[pygame.K_DOWN] != 0:
#				self.leftPaddle.rect = self.leftPaddle.rect.move(0, 4)
	
#			self.leftPaddle.tick()
#			self.rightPaddle.tick()
#			self.leftScore.tick()
#			self.rightScore.tick()
#			self.centerline.tick()
#			self.ball.tick()
#			#print self.player

#			self.screen.fill(self.black)
#			self.screen.blit(self.leftPaddle.image, self.leftPaddle.rect)
#			self.screen.blit(self.rightPaddle.image, self.rightPaddle.rect)
#			self.screen.blit(self.leftScore.text, self.leftScore.rect)
#			self.screen.blit(self.rightScore.text, self.rightScore.rect)
#			self.screen.blit(self.centerline.image, self.centerline.rect)
#			self.screen.blit(self.ball.image, self.ball.rect)	
#
#			pygame.display.flip()


#if __name__ == '__main__':
#	gs=GameSpace()
#	gs.main()

