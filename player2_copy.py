import os
from twisted.internet.protocol import Protocol, ClientFactory, Factory
from twisted.protocols.basic import LineReceiver
from twisted.internet.tcp import Port
from twisted.internet import reactor
from twisted.internet.defer import DeferredQueue

SERVER_HOST = "student02.cse.nd.edu"
PLAYER2_PORT = 50012

class PlayerTwoConnection(Protocol):

	def __init__(self, addr):
		self.addr = addr

	def connectionMade(self):
		print "Player 2 Connection Made to", SERVER_HOST, PLAYER2_PORT
	
	def dataReceived(self, data):
		print data

	def connectionLost(self, reason):
		print "Player 2 Connection Lost to", SERVER_HOST, PLAYER2_PORT

class PlayerTwoConnectionFactory(ClientFactory):

	def buildProtocol(self, addr):
		return PlayerTwoConnection(addr)

reactor.connectTCP(SERVER_HOST, PLAYER2_PORT, PlayerTwoConnectionFactory()) 
reactor.run()


