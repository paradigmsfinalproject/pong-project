import os
from twisted.internet.protocol import Protocol, ClientFactory, Factory
from twisted.protocols.basic import LineReceiver
from twisted.internet.tcp import Port
from twisted.internet import reactor
from twisted.internet import task
from twisted.internet.defer import DeferredQueue

from pygame.locals import *
from board import *

#SERVER_HOST = "fitz92"
SERVER_HOST = "student02.cse.nd.edu"
PLAYER1_PORT = 50011

class GameState(object):
	def __init__(self, connection):
		self.connection = connection
		pygame.init()
		self.size= width, height = (640, 480)
		self.screen = pygame.display.set_mode(self.size)
		self.black=(0,0,0)
		self.player = 1

		self.incrementRightScore = False
		self.incrementLeftScore = False
	
		self.leftPaddle = LeftPaddle(self)
		self.rightPaddle = RightPaddle(self)
		self.leftScore = LeftScore(self)
		self.rightScore = RightScore(self)
		self.centerline = centerLine(self)
		self.ball = Ball(self)

		reactor.callLater(0.1, self.tick)

	def tick(self):
		print "in tick"
		for event in pygame.event.get():
			if event.type == QUIT:
			#	sys.exit()
				reactor.stop()

			elif event.type == KEYDOWN:
				if event.key == K_SPACE:
					if self.ball.inMotion == False:
						self.ball.inMotion = True
					elif self.ball.inMotion == True:
						self.ball.inMotion = False
			keys = pygame.key.get_pressed()
			if keys[pygame.K_UP] != 0:
				self.connection.transport.write('up')
#				self.leftPaddle.rect = self.leftPaddle.rect.move(0, -4)
			if keys[pygame.K_DOWN] != 0:
				self.connection.transport.write('down')
#				self.leftPaddle.rect = self.leftPaddle.rect.move(0, 4)
	
	
		self.leftPaddle.tick()
		self.rightPaddle.tick()
		self.leftScore.tick()
		self.rightScore.tick()
		self.centerline.tick()
		self.ball.tick()

		self.screen.fill(self.black)
		self.screen.blit(self.leftPaddle.image, self.leftPaddle.rect)
		self.screen.blit(self.rightPaddle.image, self.rightPaddle.rect)
		self.screen.blit(self.leftScore.text, self.leftScore.rect)
		self.screen.blit(self.rightScore.text, self.rightScore.rect)
		self.screen.blit(self.centerline.image, self.centerline.rect)
		self.screen.blit(self.ball.image, self.ball.rect)
		pygame.display.flip()
		reactor.callLater(0.1, self.tick)	
			
	def updatBoard(self, data):
		if data == 'player 2 up':
			self.rightPaddle.rect = self.rightPaddle.rect.move(0, -4)
		elif data == 'player 2 down':
			self.rightPaddle.rect = self.rightPaddle.rect.move(0, 4)

#	def setConnection(self, connection):
#		self.connection = connection


class PlayerOneConnection(Protocol):
	def __init__(self, addr):
		self.addr = addr

	def connectionMade(self):
		print "Player 1 Connection Made to", SERVER_HOST, PLAYER1_PORT
		self.gs = GameState(self)
		self.lc = task.LoopingCall(self.gs.tick)
		self.lc.start(0.1)
#		self.gs.setConnection(self)

	def dataReceived(self, data):
		self.gs.updatBoard(data)
		print data

	def connectionLost(self, reason):
		print "Player 1 Connection Lost to", SERVER_HOST, PLAYER1_PORT

class PlayerOneConnectionFactory(ClientFactory):

#	def __init__(self, gs):
#		self.gs = gs

	def buildProtocol(self, addr):
		return PlayerOneConnection(addr)

if __name__ == '__main__':
#	gs = GameState()
#	lc = task.LoopingCall(gs.tick)
#	lc.start(0.1)
	reactor.connectTCP(SERVER_HOST, PLAYER1_PORT, PlayerOneConnectionFactory()) 
	reactor.run()


