import os, json
from twisted.internet.protocol import Protocol, ClientFactory, Factory
from twisted.protocols.basic import LineReceiver
from twisted.internet.tcp import Port
from twisted.internet import reactor
from twisted.internet import task
from twisted.internet.defer import DeferredQueue

from pygame.locals import *
from board import *

SERVER_HOST = "student02.cse.nd.edu"
PLAYER1_PORT = 50011

class GameState(object):
	def __init__(self, connection):
		self.connection = connection
		pygame.init()
		self.size= width, height = (640, 480)
		self.screen = pygame.display.set_mode(self.size)
		self.black=(0,0,0)
		self.player = 1
		self.state = "play"
		self.Paused = "no"
		self.inMotion = "no"
		self.spacePressed = "no"

		self.incrementRightScore = False
		self.incrementLeftScore = False
	
		self.leftPaddle = LeftPaddle(self)
		self.rightPaddle = RightPaddle(self)
		self.leftScore = LeftScore(self)
		self.rightScore = RightScore(self)

		self.centerline = centerLine(self)
		self.ball = Ball(self)

		reactor.callLater(0.01, self.tick)

		self.D = {}

	def tick(self):
		for event in pygame.event.get():
			if event.type == QUIT:
				reactor.stop()

			elif event.type == KEYDOWN:
				if event.key == K_SPACE:
					# if the user presses the space bar, this information is sent to the server
					# the server should register the space press and start the game
					self.spacePressed = "yes"
				else:
					self.spacePressed = "no"

				if event.key == K_RETURN:
					if self.Paused == "yes":
						self.state = "pause"
                                                self.Paused = "no"
                                        elif self.Paused == "no":
                                                self.Paused = "yes"

                pos = pygame.mouse.get_pos()
                y = pos[1]
		# send player1 information to the server in a JSON dictionary
		self.D['user']= "one"
                self.D['rp']= y
		self.D['lp']=self.leftPaddle.rect.y
                self.D['spacePressed']=self.spacePressed
                self.D['pause']=self.Paused
                self.rightPaddle.rect.y = y
		self.connection.sendLine(json.dumps(self.D))

		self.leftPaddle.tick()
		self.rightPaddle.tick()
		self.leftScore.tick()
		self.rightScore.tick()
		self.centerline.tick()

		self.screen.fill(self.black)
		if (self.state == "done"):
			self.screen.blit(self.over.text, self.over.rect)
		elif self.state == "pause":
			self.screen.blit(self.PauseScreen.text, self.PauseScreen.rect)
		else:
			self.screen.blit(self.leftPaddle.image, self.leftPaddle.rect)
			self.screen.blit(self.rightPaddle.image, self.rightPaddle.rect)
			self.screen.blit(self.leftScore.text, self.leftScore.rect)
			self.screen.blit(self.rightScore.text, self.rightScore.rect)
			self.screen.blit(self.centerline.image, self.centerline.rect)
			self.screen.blit(self.ball.image, self.ball.rect)
		pygame.display.flip()
		reactor.callLater(0.01, self.tick)	
			

	def updatePause(self, pause):
		if pause == True:
			self.state = "pause"
			self.PauseScreen = pauseGame(self)
		else:
			if self.state == "done":
				pass
			else:
				self.state = "play"

	def updateBall(self, x, y):
		self.ball.rect.x = x
		self.ball.rect.y = y

	def updateLeftPaddle(self, lp):
		self.leftPaddle.rect.y = lp
	

	def updateRightPaddle(self, rp):
		self.rightPaddle.rect.y = rp

	def updateLeftScore(self, ls):
		self.leftScore.num = ls
		self.leftScore.text = self.leftScore.font.render(str(self.leftScore.num), 1, (255, 255, 255))
		if int(ls) >= 10:
			self.state = "done"
			self.over = gameOverWin(self)

	def updateRightScore(self, rs):
		self.rightScore.num = rs
		self.rightScore.text = self.rightScore.font.render(str(self.rightScore.num), 1, (255, 255, 255))
		if int(rs) >= 10:
			self.state = "done"
			self.over = gameOverLose(self)

def is_json(myjson):
	try:
		json_object = json.loads(myjson)
	except ValueError, e:
		return False
	return True

class PlayerOneConnection(Protocol, LineReceiver):
	def __init__(self, addr):
		self.addr = addr

	def connectionMade(self):
		print "Player 1 Connection Made to", SERVER_HOST, PLAYER1_PORT
		self.gs = GameState(self)
		self.lc = task.LoopingCall(self.gs.tick)
		self.lc.start(0.01)

	def dataReceived(self, data):
		
		data = data.split(self.delimiter)
		for d in data:
			if (is_json(d) == True):
				# load JSON data received from server
				d=json.loads(d)
				x = d ['x']
				y= d['y']
				rp = d['rp']
				lp = d['lp']
				rs = d['rs']
				ls = d['ls']
				gsMove = d['moving']
				pause = d['pause']
		self.gs.updateRightPaddle(rp)
		self.gs.updateLeftPaddle(lp)
		self.gs.updateBall(x,y)
		self.gs.updateRightScore(rs)
		self.gs.updateLeftScore(ls)
		self.gs.updatePause(pause)

	def connectionLost(self, reason):
		print "Player 1 Connection Lost to", SERVER_HOST, PLAYER1_PORT

class PlayerOneConnectionFactory(ClientFactory):


	def buildProtocol(self, addr):
		return PlayerOneConnection(addr)

if __name__ == '__main__':
	reactor.connectTCP(SERVER_HOST, PLAYER1_PORT, PlayerOneConnectionFactory()) 
	reactor.run()


