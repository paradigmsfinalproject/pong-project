Brittany Harrington
Elizabeth Vista
Programming Paradigms Final Project

PONG

For our final project, we chose to re-create the two-player game Pong.

To initialize the game, run server.py to start the game server. Join Player 1 to the game by running player1.py, and then join
Player 2 to the game by running player2.py. 
Player 1 should be the first to connect to establish the proper order.


To start the game, either Player 1 or Player 2 presses the space bar. Player 1 controls the right paddle, and Player 2 controls the
left paddle on the screen. The paddles can move up and down, and each player controls their respective paddle by moving the mouse.
If the ball gets past the left paddle, Player 1 scores. If the ball gets past the right paddle, Player 2 scores. The scores are 
displayed at the top of the screen, and the first player to score 10 points wins the game!
The game ends once either player reaches 10 points and the players should then exit out of their respective windows to close the connection.


